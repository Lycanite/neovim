-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
local opt = vim.opt

-- clipboard
opt.clipboard = "unnamedplus" -- Sync with system clipboard.

-- Indentation
opt.shiftwidth = 4 -- 4 space indentation.
opt.tabstop = 4 -- Tabs look like 4 spaces.
opt.softtabstop = 4 -- 4 spaces inserted instead of tab.
opt.expandtab = true -- Tab will insert spaces instead.

-- Line Numbers
opt.relativenumber = false

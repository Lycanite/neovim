return {
    {
        "Rigellute/shades-of-purple.vim",
        lazy = true,
    },
    {
        "LazyVim/LazyVim",
        opts = {
            colorscheme = "shades_of_purple",
        },
    },
}
